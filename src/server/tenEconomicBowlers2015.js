const fs = require('fs'); 

function tenEconomicBowlers2015(matches, deliveries){
    let result = {};
    let currentOver = 0;

    let matchIds2015 = [];
    matches.filter((match) => {
        if (match.season == 2015) {
            matchIds2015.push(match.id);
        }
    });

    matchIds2015.filter((matchId) => {
        deliveries.filter((delivery) => {
            if (delivery.match_id == matchId) {
                if (!result[delivery.bowler]) {
                    result[delivery.bowler] = Object.create({});
                    result[delivery.bowler]["overs"] = 1;

                    result[delivery.bowler]["total_runs"] = Number(
                        delivery.total_runs
                    );

                    result[delivery.bowler]["economy"] = parseFloat(
                        Number(
                            result[delivery.bowler]["total_runs"] /
                                result[delivery.bowler]["overs"]
                        ).toFixed(2)
                    );
                } else {
                    if (currentOver != delivery.over) {
                        currentOver = delivery.over;
                        result[delivery.bowler]["overs"]++;
                    }

                    result[delivery.bowler]["total_runs"] += Number(
                        delivery.total_runs
                    );

                    result[delivery.bowler]["economy"] = parseFloat(
                        Number(
                            result[delivery.bowler]["total_runs"] /
                                result[delivery.bowler]["overs"]
                        ).toFixed(2)
                    );
                }
            }
        });
    });

    const resultEntry = Object.entries(result)
        .sort(([, a], [, b]) => a.economy - b.economy)
        .slice(0, 10); 

    const tenEconomicBowlers2015 = Object.fromEntries(resultEntry); 

    fs.writeFile(
        "../public/output/tenEconomicBowlers2015.json",
        JSON.stringify(tenEconomicBowlers2015),
        (error) => {
            if (error) throw error;
        }
    );
}

module.exports = tenEconomicBowlers2015; 