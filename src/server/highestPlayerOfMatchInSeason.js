const fs = require('fs'); 

function highestPlayerOfMatchInSeason(matches){
    
    let seasonWinnerCount = matches.reduce((acc, curr) => {
        if (acc[curr.season]){
            if(acc[curr.season][curr.player_of_match]){
                acc[curr.season][curr.player_of_match]++; 
            }
            else {
                acc[curr.season][curr.player_of_match] = 1; 
            }
        }
        else {
            acc[curr.season] = Object.create({}); 
            acc[curr.season][curr.player_of_match] = 1; 
        }
        return acc; 
    }, {}); 

    let result = Object.keys(seasonWinnerCount).reduce((acc,curr) => {
        let maxCount = 0;
        let tempPlayer = []; 

        for(let player in seasonWinnerCount[curr]){
            if(maxCount < seasonWinnerCount[curr][player]){
                maxCount = seasonWinnerCount[curr][player];
            }
        }

        for (let player in seasonWinnerCount[curr]) {
            if (seasonWinnerCount[curr][player] == maxCount) {
                tempPlayer.push(player);
            }
        } 
        
        acc[curr] = tempPlayer; 
        return acc;
    }, {}); 

    fs.writeFile('../public/output/highestPlayerOfMatchInSeason.json', JSON.stringify(result), (error) => {
        if(error) throw error; 
    }); 
}

module.exports = highestPlayerOfMatchInSeason;