const fs = require("fs");

function battingStrikeRatePerSeason(matches, deliveries) {
    
    let result = matches.reduce((acc, match) => {
        let players = {}; 
        deliveries.forEach((delivery) => {
            if (delivery.match_id === match.id && delivery.wide_runs == 0) {
                if (players[delivery.batsman]) {
                    players[delivery.batsman]["runs"] += Number(
                        delivery.batsman_runs
                    );
                    players[delivery.batsman]["balls"]++;
                    
                } else {
                    players[delivery.batsman] = Object.create({});
                    players[delivery.batsman]["runs"] = Number(delivery.batsman_runs);
                    players[delivery.batsman]["balls"] = 1;

                }
            }
        });

        if (acc[match.season]){
            for(let player in players){
                if(acc[match.season][player]){
                    acc[match.season][player].runs += players[player].runs; 
                    acc[match.season][player].balls += players[player].balls; 
                    acc[match.season][player].strike_rate = parseFloat(
                        Number(
                            100 *
                                (acc[match.season][player].runs /
                                    acc[match.season][player].balls)
                        ).toFixed(2)
                    );
                }
                else {
                    acc[match.season][player] = Object.create({}); 
                    acc[match.season][player].runs = players[player].runs; 
                    acc[match.season][player].balls = players[player].balls; 
                    acc[match.season][player].strike_rate = players[player].strike_rate; 
                }
            }
        }
        else {
            acc[match.season] = JSON.parse(JSON.stringify(players)); 
        }

        return acc; 
    }, {});

    fs.writeFile("../public/output/battingStrikeRatePerSeason.json", JSON.stringify(result), (error) => {
        if(error) throw error;
    });
}

module.exports = battingStrikeRatePerSeason;
