const fs = require("fs");

function matchesPerYear(matches) {
    let resultObj = {};
    matches.forEach((match) => {
        if (!resultObj[match.season]) {
            resultObj[match.season] = 1;
        } else {
            resultObj[match.season]++;
        }
    });

    fs.writeFile(
        "../public/output/matchesPerYear.json",
        JSON.stringify(resultObj),
        (error) => {
            if (error) throw error;
        }
    );
}

module.exports = matchesPerYear; 