const csv = require('csvtojson'); 
const matchesFilePath = '../data/matches.csv'; 
const deliveriesFilePath = '../data/deliveries.csv'; 

async function parseMatchData(){
    return await csv().fromFile(matchesFilePath); 
}

async function parseDeliveriesData(){
    return await csv().fromFile(deliveriesFilePath); 
}

async function run(){
    let matches = await parseMatchData();
    let deliveries = await parseDeliveriesData();

    const matchesPerYear = require("./matchesPerYear");
    matchesPerYear(matches);

    const matchesWonByTeamPerYear = require("./matchesWonByTeamPerYear");
    matchesWonByTeamPerYear(matches);

    const extraRunsConceded2016 = require("./extraRunsConceded2016");
    extraRunsConceded2016(matches, deliveries);

    const tenEconomicBowlers2015 = require("./tenEconomicBowlers2015");
    tenEconomicBowlers2015(matches, deliveries);

    const countTeamsWonTossAndMatch = require("./countTeamsWonTossAndMatch");
    countTeamsWonTossAndMatch(matches); 

    const highestPlayerOfMatchInSeason = require("./highestPlayerOfMatchInSeason");
    highestPlayerOfMatchInSeason(matches); 

    const playerDismissalCount = require("./playerDismissalCount"); 
    playerDismissalCount(deliveries); 

    const economicSuperOverBowler = require("./economicSuperOverBowler");
    economicSuperOverBowler(deliveries); 

    const battingStrikeRatePerSeason = require("./battingStrikeRatePerSeason");
    battingStrikeRatePerSeason(matches, deliveries); 
}

run(); 