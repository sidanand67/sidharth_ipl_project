const fs = require('fs'); 

function playerDismissalCount(deliveries){

    let result = deliveries.reduce((acc, curr) => {
        if (curr.player_dismissed){
            if(acc[curr.player_dismissed]){
                acc[curr.player_dismissed]++; 
            }
            else {
                acc[curr.player_dismissed] = 1; 
            }
        }
        return acc;
    }, {}); 

    fs.writeFile('../public/output/playerDismissalCount.json', JSON.stringify(result), (error) => {
        if(error) throw error; 
    });
}

module.exports = playerDismissalCount; 