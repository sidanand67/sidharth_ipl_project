const fs = require("fs");

function matchesWonByTeamPerYear(matches) {
    let result = {};

    matches.forEach((match) => {
        if (match.team1 && match.team2) {
            if (!result[match.team1]) {
                result[match.team1] = Object.create({});
                result[match.team1][match.season] =
                    match.winner === match.team1 ? 1 : 0;
            } else {
                if (result[match.team1][match.season]) {
                    if (match.winner === match.team1) {
                        result[match.team1][match.season]++;
                    }
                } else {
                    result[match.team1][match.season] =
                        match.winner === match.team1 ? 1 : 0;
                }
            }
            if (!result[match.team2]) {
                result[match.team2] = Object.create({});
                result[match.team2][match.season] =
                    match.winner === match.team2 ? 1 : 0;
            } else {
                if (result[match.team2][match.season]) {
                    if (match.winner === match.team2) {
                        result[match.team2][match.season]++;
                    }
                } else {
                    result[match.team2][match.season] =
                        match.winner === match.team2 ? 1 : 0;
                }
            }
        }
    });

    fs.writeFile(
        "../public/output/matchesWonByTeamsPerYear.json",
        JSON.stringify(result),
        (error) => {
            if (error) throw error;
        }
    );
}

module.exports = matchesWonByTeamPerYear;
