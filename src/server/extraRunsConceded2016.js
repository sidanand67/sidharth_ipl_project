const fs = require('fs'); 

function extraRunsConceded2016(matches, deliveries){
    let result = {};

    let matchIds2016 = [];
    matches.filter((match) => {
        if (match.season == 2016) {
            matchIds2016.push(match.id);
        }
    });

    matchIds2016.forEach((matchId) => {
        deliveries.filter((delivery) => {
            if (delivery.match_id == matchId) {
                if (!result[delivery.bowling_team]) {
                    result[delivery.bowling_team] = Number(delivery.extra_runs);
                } else {
                    result[delivery.bowling_team] += Number(
                        delivery.extra_runs
                    );
                }
            }
        });
    });

    fs.writeFile(
        "../public/output/extraRunsConceded2016.json",
        JSON.stringify(result),
        (error) => {
            if (error) throw error;
        }
    );
}   

module.exports = extraRunsConceded2016;