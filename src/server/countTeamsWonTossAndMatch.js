const fs = require('fs'); 

function countTeamsWonTossAndMatch(matches){
    let result = matches.reduce((acc, curr) => {
        if (curr.toss_winner === curr.winner){
            if(acc[curr.winner]){
                acc[curr.winner]++; 
            }
            else {
                acc[curr.winner] = 1; 
            }
        }
        return acc;
    }, {}); 

    fs.writeFile('../public/output/countTeamsWonTossAndMatch.json', JSON.stringify(result), (error) => {
        if(error) throw error; 
    }); 
}

module.exports = countTeamsWonTossAndMatch; 