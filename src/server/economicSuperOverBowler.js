const fs = require("fs");

function economicSuperOverBowler(deliveries) {
    let currentOver = 0;
    let superOvers = deliveries.reduce((acc, curr) => {
        if (curr.is_super_over === "1") {
            if (acc[curr.bowler]) {
                if (currentOver != curr.over) {
                    acc[curr.bowler]["overs"]++;
                }
                acc[curr.bowler]["runs"] += Number(curr.total_runs);
                acc[curr.bowler]["economy"] = parseFloat(
                    Number(
                        acc[curr.bowler]["runs"] / acc[curr.bowler]["overs"]
                    ).toFixed(2)
                );
            } else {
                acc[curr.bowler] = Object.create({});
                acc[curr.bowler]["runs"] = Number(curr.total_runs);
                acc[curr.bowler]["overs"] = 1;
                acc[curr.bowler]["economy"] = Number(curr.total_runs);
            }
        }
        return acc;
    }, {});

    const resultEntry = Object.entries(superOvers)
        .sort(([,a], [,b]) => a.economy - b.economy)
        .slice(0,1);
        
    const resultObj = Object.fromEntries(resultEntry);     

    fs.writeFile("../public/output/economicSuperOverBowler.json", JSON.stringify(resultObj), (error) => {
        if(error) throw error;
    });
}

module.exports = economicSuperOverBowler;
